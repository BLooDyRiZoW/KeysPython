import math
class Triangle:
    def __init__(self, _ax, _ay, _bx, _by, _cx, _cy):
        self.ax = _ax
        self.ay = _ay
        self.bx = _bx
        self.by = _by
        self.cx = _cx
        self.cy = _cy
    def S(self):
        return (math.fabs(float(0.5*(self.ax*(self.by-self.cy)+self.bx*(self.cy-self.ay)+self.cx*(self.ay-self.by)))))
    def P(self):
        AB = math.sqrt(math.pow((self.bx-self.ax), 2)+math.pow((self.by-self.ay), 2))
        BC = math.sqrt(math.pow((self.bx-self.cx), 2)+math.pow((self.by-self.cy), 2))
        CA = math.sqrt(math.pow((self.cx-self.ax), 2)+math.pow((self.cy-self.ay), 2))
        return (float(AB+BC+CA))
    def M(self):
        X = (self.ax+self.bx+self.cx)/3
        Y = (self.ay+self.by+self.cy)/3
        return(X, Y)
T = Triangle(int(input('Введите x вершины A: ')), int(input('Введите y вершины A: ')),int(input('Введите x вершины B: ')), int(input('Введите y вершины B: ')), int(input('Введите x вершины C: ')), int(input('Введите y вершины C: ')))
print(T.S())
print(T.P())
print(T.M())
