class MyClass:
    a = 3
    b = 2
    def __str__(self):
        print(MyClass.a, MyClass.b)
    def set(self, _a, _b):
        MyClass.a = _a
        MyClass.b = _b
    def sum(self):
        return (MyClass.a+MyClass.b)
    def max(self):
        if MyClass.a>MyClass.b: return MyClass.a
        else: return MyClass.b
obj = MyClass()
obj.__str__()
obj.set(int(input('Первая переменная: ')), int(input('Вторая переменная: ')))
obj.__str__()
print('Сумма:', obj.sum())
print('Максимальное:', obj.max())


